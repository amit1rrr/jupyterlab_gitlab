import {
  ILayoutRestorer, JupyterLab, JupyterLabPlugin
} from '@jupyterlab/application';

import {
  IDocumentManager
} from '@jupyterlab/docmanager';

import {
  IFileBrowserFactory
} from '@jupyterlab/filebrowser';

import {
  GitLabDrive
} from './contents';

import {
  GitLabFileBrowser
} from './browser';

import {
  ISettingRegistry
} from '@jupyterlab/coreutils';

import '../style/index.css';

/**
 * GitLab filebrowser plugin state namespace.
 */
const NAMESPACE = 'gitLab-filebrowser';

/**
 * The JupyterLab plugin for the GitLab Filebrowser.
 */
const fileBrowserPlugin: JupyterLabPlugin<void> = {
  id: '@jupyterlab/gitlab:gitlab',
  requires: [IDocumentManager, IFileBrowserFactory, ILayoutRestorer, ISettingRegistry],
  activate: activateFileBrowser,
  autoStart: true
};

/**
 * Activate the file browser.
 */
function activateFileBrowser(app: JupyterLab, manager: IDocumentManager, factory: IFileBrowserFactory, restorer: ILayoutRestorer, settingRegistry: ISettingRegistry): void {
  const { commands } = app;
  const id = fileBrowserPlugin.id;

  // Add the Google Drive backend to the contents manager.
  const drive = new GitLabDrive(app.docRegistry);
  manager.services.contents.addDrive(drive);

  const browser = factory.createFileBrowser(NAMESPACE, {
    commands,
    driveName: drive.name,
    state: null
  });

  browser.model.cd('/');

  let userName = '';
  let userCode = '';
  let groupName = '';
  settingRegistry.load(id).then(settings => {
    userName = settings.get('userName').composite as string;
    gitLabBrowser.userName.name.set(userName);
    console.warn('User name = ' + userName);
    userCode = settings.get('userCode').composite as string;
    gitLabBrowser.userCode.name.set(userCode);
    console.warn('User code = ' + userCode);
    groupName = settings.get('groupName').composite as string;
    gitLabBrowser.groupName.name.set(groupName);
    console.warn('Group name = ' + groupName);
  });

  const gitLabBrowser = new GitLabFileBrowser(browser, drive, userName, userCode, groupName);

  gitLabBrowser.title.iconClass = 'jp-GitLab-tablogo';
  gitLabBrowser.id = 'gitLab-file-browser';

  // Add the file browser widget to the application restorer.
  restorer.add(gitLabBrowser, NAMESPACE);
  app.shell.addToLeftArea(gitLabBrowser, { rank: 102 });

  return;
}

export default fileBrowserPlugin;
