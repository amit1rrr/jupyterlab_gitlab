import {
  URLExt
} from '@jupyterlab/coreutils';

import {
  ServerConnection
} from '@jupyterlab/services';

import {
  GitLabFileBrowser
} from './browser';

export
const GITLab_API = 'https://gitlab.com/api/v4';

/**
 * Make a client-side request to the GitLib API.
 *
 * @param url - the api path for the GitLib API v4
 *   (not including the base url).
 *
 * @returns a Promise resolved with the JSON response.
 */
export
function browserApiRequest<T>(url: string): Promise<T> {
  let key = '';
  if (GitLabFileBrowser.private_key !== '') {
    if (url.indexOf("?") === -1) {
      key = '?private_token=' + GitLabFileBrowser.private_key;
    } else {
      key = '&private_token=' + GitLabFileBrowser.private_key;
    }
  }
  
  const requestUrl = URLExt.join(GITLab_API, url) + key;
  return window.fetch(requestUrl).then(response => {
    if (response.status !== 200) {
      return response.json().then(data => {
        throw new ServerConnection.ResponseError(response, data.message);
      });
    }
    return response.json();
  });
}

/**
 * Typings representing contents from the GitLab API v4.
 */
export
class GitLabContents {
  type: 'file' | 'dir' | 'submodule' | 'symlink' | 'tree' | 'blob';
  id: string;
  name: string;
  mode: string;
  path: string;
}

/**
 * Typings representing file contents from the GitLab API v4.
 */
export
class GitLabFileContents extends GitLabContents {
  type: 'file';
  encoding: 'base64';
  content?: string;
}

/**
 * Typings representing a directory from the GitLab API v4.
 */
export
class GitLabDirectoryContents extends GitLabContents {
  type: 'dir';
}

/**
 * Typings representing a blob from the GitLab API v4.
  */
export
class GitLabBlob {
  content: string;
  encoding: 'base64';
  url: string;
  sha: string;
  size: number;
}


/**
 * Typings representing symlink contents from the GitLab API v4.
 */
export
class GitLabSymlinkContents extends GitLabContents {
  type: 'symlink';
}

/**
 * Typings representing submodule contents from the GitLab API v4.
 */
export
class GitLabSubmoduleContents extends GitLabContents {
  type: 'submodule';
}

/**
 * Typings representing directory contents from the GitLab API v4.
 */
export
type GitLabDirectoryListing = GitLabContents[];

/**
 * Typings representing repositories from the GitLab API v4.
 *
 * #### Notes
 *   This is incomplete.
 */
export
class GitLabRepo {
  id: number;
  owner: any;
  name: string;
  name_with_namespace: string;
  description: string;
  web_url: string;
}
